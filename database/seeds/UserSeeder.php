<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Usuario #1', 
            'email' => 'usuario1@domain.com', 
            'password' => Hash::make('usuario1'),
            'rol' => 1
        ]);

        User::create([
            'name' => 'Usuario #2', 
            'email' => 'usuario2@domain.com', 
            'password' => Hash::make('usuario2'),
            'rol' => 2
        ]);

        User::create([
            'name' => 'Usuario #2', 
            'email' => 'usuario3@domain.com', 
            'password' => Hash::make('usuario3'),
            'rol' => 3
        ]);

        User::create([
            'name' => 'Admin', 
            'email' => 'admin@domain.com', 
            'password' => Hash::make('admin'),
            'rol' => 4
        ]);
    }
}
