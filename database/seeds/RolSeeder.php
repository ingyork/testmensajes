<?php

use App\Rol;
use Illuminate\Database\Seeder;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rol::create([
            'nombre' => 'Carranga'
        ]);

        Rol::create([
            'nombre' => 'Vallenato'
        ]);

        Rol::create([
            'nombre' => 'Reggaeton'
        ]);

        Rol::create([
            'nombre' => 'Admin'
        ]);


    }
}
