<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateUser()
    {
        $this->visit('/')
            ->type('usuario1@domain.com', 'email')
            ->type('usuario1', 'password')
            ->press('submit')
            ->seePageIs('/welcome')
            ->see('Hola! Usuario #1');
    }
}
