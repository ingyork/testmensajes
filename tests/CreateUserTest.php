<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreateUserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateUser()
    {
        $this->visit('admin/user/register')
            ->type('3', 'rol')
            ->type('Jorge Castro Test', 'name')
            ->type('jorge.castrom@cecar.edu.co', 'email')
            ->type(\Hash::make('123'), 'password')
            ->press('bntCreateUser')
            ->visit('admin/user/register/C/1')
            ->see('Se ha registrado el usuario satisfactoriamente');
    }

}
