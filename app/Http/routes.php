<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'AuthController@show');
Route::get('welcome', 'HomeController@welcome');
Route::post('login', 'AuthController@login');
Route::get('logout', 'AuthController@logout');



Route::group(['prefix' => 'admin'], function(){
    Route::get('home', 'HomeController@home');
    Route::get('user/register/{c?}/{status?}', 'UserController@newUser')->where(['c' => '[c,r,u,d,C,R,U,D]', 'status' => '-?[0-9]+']);;
    Route::post('user/register', 'UserController@register');
});
