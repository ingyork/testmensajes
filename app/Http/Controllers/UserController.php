<?php

namespace App\Http\Controllers;


use App\Http\Requests\StoreUser;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    
    public function newUser($c=null, $status=null)
    {
        $isSaved = ($c == null) ? false : true;
        $roles = \DB::connection()->select("select * from rols where id<>4");
        
        return view('user.new-user')
            ->with('roles', $roles)
            ->with('isSaved', $isSaved)
            ->with('crud', $c)
            ->with('status', $status);
    }


    public function register(StoreUser $request)
    {

        $crud = "C";
        $status = 0;

        //$user = User::create($request->all());
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => \Hash::make($request->password),
            'rol' => $request->rol
        ]);
        
        if($user != null)
        {
            $status = 1;
        }

        return redirect()->action('UserController@newUser', [$crud, $status]);
    }
}
