<?php

namespace App\Http\Controllers;


use Auth;
use App\MensajesRol;
use Session;
use Cache;
use Illuminate\Http\Request;

class AuthController extends Controller
{

	/*Metodo para mostrar la vista del login*/
    public function show()
    {
        return view("auth.login");
    }


	/*Metodo para autenticarse*/
    public function login(Request $request)
    {

        $rules = array(
			'email' => 'required',
			'password' => 'required'
		);

		
		$messages = array(
		    'email.required' => 'El email es requerido',
		    'password.required' => 'La contraseña es requerida'
		);
		
		$this->validate($request, $rules, $messages);
		

		$userdata = array(
			'email' => $request['email'],
			'password' => $request['password']
		);

		if(Auth::attempt($userdata)){

			if(Auth::user()->rol == 4)
				return redirect()->intended('/admin/home');	
			else
				return redirect()->intended('/welcome');
				
		}
		else{

			return redirect()->back()
				->withErrors('Email o contraseña incorrectos')
				->withInput(\Request::except('password'));
		}

    }


	/*Metodo para cerrar Sesión*/
	public function logout()
	{
		Auth::logout();
        Session::flush();
		Cache::flush();
        return redirect('/');
	}



}
