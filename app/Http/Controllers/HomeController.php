<?php

namespace App\Http\Controllers;


use Auth;
use App\MensajesRol;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    

    /*Metodo para mostrar la vista a los usuarios normales*/
    public function welcome(Request $request)
    {
        $rolUser = Auth::user()->rol;
        $mensaje = MensajesRol::where('rol', $rolUser)->get();
        
        return view('welcome')
            ->with('nombreUsuario', Auth::user()->name)
            ->with('mensaje', $mensaje[0]);
    }


    /*Metodo para mostrar la vista Home al administrador*/
    public function home()
    {
        return view('home.home');
    }


}
