<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'rol' => 'required|not_in:',
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed'
        ];
    }



    public function messages()
    {
        return [
        	'rol.required'             => 'El rol es obligatorio',
        	'rol.not_in:'              => 'El rol es obligatorio',
			'name.required'            => 'El nombre es obligatorio',
			'password.required'        => 'La contraseña es obligatorio',
			'password.confirmed'       => 'Las confirmación de la contraseña no coincide',
			'email.required'           => 'EL email es obligatorio',
			'email.unique'             => 'El email ya esta registrado en la base de datos'
        ];
    }



}
