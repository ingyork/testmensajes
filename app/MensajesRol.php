<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MensajesRol extends Model
{
    
    protected $table = 'mensajes_rol';
	protected $primaryKey = 'id';

	protected $fillable = ['mensaje', 'rol'];

}
