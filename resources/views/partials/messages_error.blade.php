@if($errors->any())

  <div class="row" id="capaErrores">
    <div class="alert alert-danger">
      <strong>Errores!</strong>

      <p>Por favor corrige los errores</p>
      <ul>
      
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach

      </ul>

    </div>
  </div>

@endif