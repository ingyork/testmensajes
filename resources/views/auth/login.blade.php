<!DOCTYPE html>
<html lang='es'>
<head>
    <meta charset="UTF-8" /> 
    <title>
        HTML Document Structure
    </title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/login.css') }}" />
</head>
<body> <!-- Copy the snippet beween the body tags and use it in your website, if required -->

<form id="form" name="form" action="{{ url('login') }}" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div id="block">
		<label id="user" for="name">p</label>
		<input type="text" name="email" id="email" placeholder="Email" value="{{ old('email') }}" autofocus required/>
		<label id="pass" for="password">k</label>
		<input type="password" name="password" id="password" placeholder="Password" required />
		<input type="submit" id="submit" name="submit" value="a"/>
	</div>
</form>
<div id="option"> 
	<p>Login</p> 
	<!--<a href="#">forgot?</a>-->
</div>

<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-4 col-md-offset-4">
			@include('partials.messages_error')
		</div>
	</div>
</div>
			


</body>
</html>
