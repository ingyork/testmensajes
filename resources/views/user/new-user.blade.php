@extends('main-template-adminlte')

@section('breadcrum')
<li><a href="#">Usuario</a></li>
<li class="active">Nuevo</li>
@stop

@section('contenido')


<div class="box">
    <div class="box-body">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Registro de Usuario</h1>
                
                <form method="post" action="{{ url('admin/user/register') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">


                    @if($isSaved)

                        @if($crud == "C")
                            @if($status)
                            <div class="alert alert-success">
                                Se ha registrado el usuario satisfactoriamente
                            </div>
                            @endif
                        @endif

                        <br><br>

                    @endif



                    <div class="row">
                        <div class="col-xs-12">
                            @include('partials.messages_error')
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="form-group">
                                <label for="rol">Rol</label>
                                <select class="form-control" id="rol" name="rol">
                                    @foreach($roles as $rol)
                                    <option value="{{ $rol->id }}">{{ $rol->nombre }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="name">Nombre Completo</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Digite su nombre completo" autofocus />
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" placeholder="Digite su Email"/>
                            </div>
                        </div>


                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="password">Contraseña</label>
                                <input type="password" class="form-control" id="password" name="password" value="" placeholder="Digite la contraseña" />
                            </div>
                        </div>


                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="password">Confirme Contraseña</label>
                                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" value="" placeholder="Digite la contraseña" />
                            </div>
                        </div>



                    </div>


                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-success" name="bntCreateUser"><i class="fa fa-save"></i> Registrar Usuario</button>
                        </div>
                    </div>
                    

                </form>

                    
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        
    </div>
</div>

@stop